const SauceService = require("../services/sauce.service");
const SauceController = {
  findAll: async (req, res, next) => {
    const sauces = await SauceService.findAll();
    res.status(200).send(sauces);
  },
  findById: async (req, res, next) => {
    const sauceId = req.params.id;
    const sauce = await SauceService.findById(sauceId);
    res.status(200).send(sauce);
  },
  findByName: async (req, res, next) => {
    const query = req.query.q;
    const sauce = await SauceService.findByName(query);
    res.status(200).send(sauce);
  },
  create: async (req, res, next) => {
    const sauce = req.body;
    await SauceService.create(sauce);
    res.status(200).send({ message: "sauce created successfully" });
  },
  update: async (req, res, next) => {
    const sauceId = req.params.id;
    const sauceInfo = req.body;
    try {
      await SauceService.update(sauceId, sauceInfo);
      res.status(200).send({ message: "sauce updated successfully" });
    } catch (error) {
      res.status(404).send({ message: `sauce with id - ${sauceId} not found` });
    }
  },
  delete: async (req, res, next) => {
    const sauceId = req.params.id;
    try {
      await SauceService.delete(sauceId);
      res.status(200).send({ message: "sauce deleted successfully" });
    } catch (error) {
      res.status(404).send({ message: `sauce with id - ${sauceId} not found` });
    }
  },
};

module.exports = SauceController;
