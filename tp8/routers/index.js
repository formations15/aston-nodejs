const express = require("express");
const router = express.Router();
const SauceRouter = require("./souces.router");
const UsersRouter = require("./users.router");
const API_SAUCES = `/sauces`;

router.use(API_SAUCES, SauceRouter);

router.use(`/users`, UsersRouter);

module.exports = router;
