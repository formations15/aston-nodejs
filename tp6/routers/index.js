const express = require("express");
const router = express.Router();

const SauceRouter = require("./sauces.router");

router.use("/sauces", SauceRouter);

module.exports = router;
