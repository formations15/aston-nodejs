const SaucesRepository = require("../repositories/sauces.repository");
const SauceRepository = require("../repositories/sauces.repository");
const sauceModel = require("../models/sauce.model");
const SaucesServices = {
  // return result with callback
  getSauces: (callback) => {
    return SauceRepository.getSauces((result) => {
      callback(result);
    });
  },
  // handle promise with .then .catch and return result with callback
  findById: (id, callback) => {
    SauceRepository.findById(id)
      .then((result) => callback(null, result))
      .catch((err) => callback(err, null)); // i/O event
    return;
  },
  findByIdAysnc: async (id) => {
    const sauce = await SauceRepository.findById(id);
    return sauce;
  },
  // handle promise with async await and return result
  findByName: async (name) => {
    try {
      const sauce = await SauceRepository.findByName(name); // I/O event
      return sauce;
    } catch (error) {
      console.log(error);
    }
  },
  createSauce: async (data) => {
    const sauce = new sauceModel({
      name: data.name,
      note: data.note,
      description: data.description,
    });
    const createdSauce = await SaucesRepository.createSauce(sauce);
    return createdSauce;
  },
  updateSauce: async (id, data) => {
    const sauce = new sauceModel({
      name: data.name,
      note: data.note,
      description: data.description,
    });
    const modifiedSauce = await SaucesRepository.updateSauce(id, sauce);
    return modifiedSauce;
  },
  deleteSauce: async (id) => {
    await SaucesRepository.deleteSauce(id);
    return;
  },
};

module.exports = SaucesServices;
