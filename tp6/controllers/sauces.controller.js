const SaucesService = require("../services/sauces.service");

const SaucesController = {
  getSauces: (req, res, next) => {
    SaucesService.getSauces((sauces) => {
      res.status(200).json(sauces);
    });

    return;
  },
  findById: (req, res, next) => {
    const id = req.params.id;
    if (!id) {
      res.status(400).json({ message: "id is not defined add id" });
    }

    SaucesService.findById(id, (sauce) => {
      if (sauce) {
        res.status(200).json({ sauce: sauce });
      } else {
        res.status(404).json({ message: "sauce not found" });
      }
    });

    return;
  },
  findByName: async (req, res, next) => {
    const name = req.query.q;
    if (!name) {
      res.status(400).json({ message: "query is not defined add q=name" });
    }
    const sauce = await SaucesService.findByName(name); // because this a async function
    if (sauce) {
      res.status(200).json({ sauce: sauce });
    } else {
      res.status(404).json({ message: "sauce not found" });
    }
    return;
  },
  createSauce: async (req, res, next) => {
    const body = req.body;
    if (!body || body != {}) {
      res.status(400).json({ message: "body is not defined" });
      return;
    }

    try {
      const sauce = await SaucesService.createSauce(body);
      if (sauce) {
        res.status(200).json({ sauce: sauce });
        return;
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "sauce not created" });
    }
  },
  updateSauce: async (req, res, next) => {
    const id = req.params.id;
    const body = req.body;
    console.log(body);
    if (!body) {
      res.status(400).json({ message: "body is not defined" });
      return;
    }
    try {
      const sauce = await SaucesService.updateSauce(id, body);
      if (sauce) {
        res.status(200).json({ message: "sauce updated successfully" });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "sauce not updated" });
    }
  },
  deleteSauce: async (req, res, next) => {
    const id = req.params.id;
    const sauce = await SaucesService.findByIdAysnc(id);
    if (!sauce) {
      res.status(404).json({ message: `sauce not found with id ${id}` });
      return;
    }
    try {
      await SaucesService.deleteSauce(id);
      res.status(200).json({ message: "sauce deleted successfully" });
      return;
    } catch (error) {
      res.status(500).json({ message: "internal server error" });
      return;
    }
  },
};

module.exports = SaucesController;
