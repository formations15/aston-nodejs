const express = require("express");
const app = express();
const db = require("./models/db");
const router = require("./routers");

(async () => {
  await db.sequelize.sync();
})();
app.use(express.json());
app.use("/api", router);

app.get("*", (req, res, next) => {
  res.status(404).json({ message: "api not found" });
  return;
});

app.listen(process.env.PORT || "3000", () => {
  if (process.env.PORT)
    console.log(`le serveur est à l'ecoute sur le port ${process.env.PORT}`);
  else console.log(`le serveur est à l'ecoute sur le port 3000`);
});
