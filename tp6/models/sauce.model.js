module.exports = (sequelize, Sequelize) => {
  const Sauce = sequelize.define("sauce", {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true,
    },
    nom: {
      type: Sequelize.STRING,
    },
    note: {
      type: Sequelize.INTEGER,
    },
    description: {
      type: Sequelize.STRING,
    },
  });

  return Sauce;
};
