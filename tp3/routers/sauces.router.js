const express = require("express");
const SaucesController = require("../controllers/sauces.controller");
const sauceRouter = express.Router();

sauceRouter.route("/").get(SaucesController.getSauces);

sauceRouter.route("/search").get(SaucesController.findByName);

sauceRouter.route("/:id").get(SaucesController.findById);

module.exports = sauceRouter;
