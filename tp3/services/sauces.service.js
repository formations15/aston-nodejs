const SauceRepository = require("../repositories/sauces.repository");

const SaucesServices = {
  getSauces: () => {
    return SauceRepository.getSauces();
  },
  findById: (id) => {
    return SauceRepository.findById(id);
  },
  findByName: (name) => {
    return SauceRepository.findByName(name);
  },
};

module.exports = SaucesServices;
