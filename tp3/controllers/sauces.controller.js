const SaucesService = require("../services/sauces.service");

const SaucesController = {
  getSauces: (req, res, next) => {
    const sauces = SaucesService.getSauces();
    res.status(200).json(sauces);
    return;
  },
  findById: (req, res, next) => {
    const id = req.params.id;
    if (!id) {
      res.status(400).json({ message: "id is not defined add id" });
    }

    const sauce = SaucesService.findById(id);
    if (sauce) {
      res.status(200).json({ sauce: sauce });
    } else {
      res.status(404).json({ message: "sauce not found" });
    }
    return;
  },
  findByName: (req, res, next) => {
    const name = req.query.q;
    if (!name) {
      res.status(400).json({ message: "query is not defined add q=name" });
    }
    const sauce = SaucesService.findByName(name);
    if (sauce) {
      res.status(200).json({ sauce: sauce });
    } else {
      res.status(404).json({ message: "sauce not found" });
    }
    return;
  },
};

module.exports = SaucesController;
