const mocks = require("../mocks/sauces");

const SaucesRepository = {
  getSauces: () => {
    return mocks.SAUCES;
  },
  findById: (id) => {
    return mocks.SAUCES.find((sauce) => sauce.id == id);
  },
  findByName: (name) => {
    return mocks.SAUCES.find((sauce) => sauce.nom == name);
  },
};

module.exports = SaucesRepository;
