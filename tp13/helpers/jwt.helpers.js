const jwt = require("jsonwebtoken");

const SECRET_KEY = "1234";
const JwtHelper = {
  getAcessToken: function (user) {
    const token = jwt.sign(user, SECRET_KEY, {
      expiresIn: "3h",
    });
    return token;
  },
};

module.exports = JwtHelper;
