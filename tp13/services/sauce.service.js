const SauceRepository = require("../repositories/sauce.repository");

/**
 * Sauce service
 */

const SauceService = {
  findAll: async () => {
    return await SauceRepository.findAll();
  },
  findById: async (id) => {
    const sauce = await SauceRepository.findById(id);
    return sauce;
  },
  findByName: async (name) => {
    const sauce = await SauceRepository.findByName(name);
    return sauce;
  },
  create: async (sauce) => {
    await SauceRepository.create(sauce);
  },
  update: async (id, sauceInfo) => {
    const sauce = await SauceRepository.findById(id);
    if (!sauce) {
      throw Error("sauce not found");
    }
    await SauceRepository.update(id, sauceInfo);
  },
  delete: async (id) => {
    const sauce = await SauceRepository.findById(id);
    if (!sauce) {
      throw Error("sauce not found");
    }
    return await SauceRepository.delete(id);
  },
};

module.exports = SauceService;
