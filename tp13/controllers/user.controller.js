const UserService = require("../services/user.service");
const DuplicatedUserError = require("../errors/duplicatedUserError");
const UserController = {
  findAll: async function (req, res, next) {
    const users = await UserService.findAll();

    res.status(200).json({ users: users });
    return;
  },
  createUser: async function (req, res, next) {
    const user = req.body;
    try {
      await UserService.createUser(user);
      res.status(201).json({ message: "User created successfully" });
      return;
    } catch (error) {
      if (error instanceof DuplicatedUserError) {
        res.status(409).json({ message: "username already taken" });
        return;
      } else {
        res.status(500).json({ message: "internel server error" });
        return;
      }
    }
  },
};

module.exports = UserController;
