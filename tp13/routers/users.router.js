const express = require("express");
const usersRouter = express.Router();
const UserController = require("../controllers/user.controller");
const midd = require("../middlewares/middlewares");
const passport = require("passport");

const { validate } = require("express-validation");
const UsersValidators = require("../validators/users.validators");
usersRouter
  .route("/")
  .get(passport.authenticate("jwt"), midd.IsAdmin, UserController.findAll)
  .post(
    validate(UsersValidators.validateCreation, {}, {}),
    UserController.createUser
  );

module.exports = usersRouter;
