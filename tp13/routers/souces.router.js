const express = require("express");
const sauceRouter = express.Router();
const SauceController = require("../controllers/sauce.controller");
const { validate } = require("express-validation");
const SaucesValidators = require("../validators/sauces.validators");
const passport = require("passport");
const API_SAUCES_PARAM = `/:id`;
const API_SAUCES_QUERY = `/search`;

// Begin Router
sauceRouter
  .route("/")
  .all(passport.authenticate("jwt"))
  .get(SauceController.findAll)
  .post(
    validate(SaucesValidators.validateCreation, {}, {}),
    SauceController.create
  );

sauceRouter
  .route(API_SAUCES_QUERY)
  .get(passport.authenticate("jwt"), SauceController.findByName);

sauceRouter
  .route(API_SAUCES_PARAM)
  .all(passport.authenticate("jwt"))
  .get(SauceController.findById)
  .put(
    validate(SaucesValidators.validateUpdate, {}, {}),
    SauceController.update
  )
  .delete(SauceController.delete);

// end Router
module.exports = sauceRouter;
