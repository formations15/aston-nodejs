const { Joi } = require("express-validation");

const SaucesValidators = {
  validateCreation: {
    body: Joi.object({
      name: Joi.string().max(75).required(),
      note: Joi.number().integer().min(0).max(20).required(),
      description: Joi.string().max(250),
    }),
  },
  validateUpdate: {
    params: Joi.object({
      id: Joi.string().guid().required(),
    }),
    body: Joi.object({
      name: Joi.string().max(75).required(),
      note: Joi.number().integer().min(0).max(20).required(),
      description: Joi.string().max(250),
    }),
  },
};

module.exports = SaucesValidators;
