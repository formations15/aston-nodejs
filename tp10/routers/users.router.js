const express = require("express");
const usersRouter = express.Router();
const UserController = require("../controllers/user.controller");

const { validate } = require("express-validation");
const UsersValidators = require("../validators/users.validators");
usersRouter
  .route("/")
  .get(UserController.findAll)
  .post(
    validate(UsersValidators.validateCreation, {}, {}),
    UserController.createUser
  );

module.exports = usersRouter;
