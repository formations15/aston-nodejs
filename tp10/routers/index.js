const express = require("express");
const router = express.Router();
const SauceRouter = require("./souces.router");
const UsersRouter = require("./users.router");

const API_SAUCES = `/sauces`;
const API_USERS = `/users`;

router.use(API_SAUCES, SauceRouter);
router.use(API_USERS, UsersRouter);

module.exports = router;
