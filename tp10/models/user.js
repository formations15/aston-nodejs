module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true,
    },
    username: {
      type: Sequelize.STRING(80),
    },
    password: {
      type: Sequelize.STRING(80),
    },
    role: { type: Sequelize.ENUM, values: ["USER", "ADMIN"] },
  });

  return User;
};
