const express = require("express");
const app = express();
const { ValidationError } = require("express-validation");
const indexRouter = require("./routers/index");
const middlewares = require("./middlewares/middlewares");
const db = require("./models");
//  test
(async () => {
  await db.sequelize.sync();
})();

app.use(middlewares.logDate);
app.use(middlewares.logMethodeType);

app.use(express.json());
app.use("/api", indexRouter);

app.use(function (err, req, res, next) {
  if (err instanceof ValidationError) {
    return res.status(err.statusCode).json(err);
  }

  return res.status(500).json(err);
});
app.listen(process.env.PORT || "3000", () => {
  console.log("Le serveur est à l’écoute sur le port 3000");
});
