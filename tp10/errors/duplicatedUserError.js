class DuplicatedUserError extends Error {
  constructor() {
    super();
    Object.setPrototypeOf(this, DuplicatedUserError.prototype);
  }
}

module.exports = DuplicatedUserError;
