const db = require("../models");
const Sauce = db.sauces;
/**
 * Sauce repository
 */
const SauceRepository = {
  findAll: () => {
    return Sauce.findAll();
  },
  findById: (id) => {
    const sauce = Sauce.findByPk(id);
    return sauce;
  },
  findByName: (name) => {
    const sauce = Sauce.findOne({ where: { nom: name } });
    return sauce;
  },
  create: (sauce) => {
    return Sauce.create(sauce);
  },
  update: (id, sauce) => {
    return Sauce.update(sauce, { where: { id: id } });
  },
  delete: (id) => {
    return Sauce.destroy({ where: { id: id } });
  },
};

module.exports = SauceRepository;
