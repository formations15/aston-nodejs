const express = require("express");
const usersRouter = express.Router();
const middlewares = require("../middlewares/middlewares");

usersRouter
  .route("/")

  .post(
    middlewares.auth,

    (req, res, next) => {
      res.status(200).send("user created successfully");
      return;
    }
  );

module.exports = usersRouter;
