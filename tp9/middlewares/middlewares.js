exports.logDate = function (req, res, next) {
  console.log(`current date: ${Date.now()}`);
  next();
};

exports.logMethodeType = (req, res, next) => {
  console.log(`methode type: ${req.method}`);
  next();
};

exports.auth = (req, res, next) => {
  const auth = req.headers["authorization"];

  if (auth) {
    next();
  } else {
    res.status(401).send("Unauthorized");
    return;
  }
};
