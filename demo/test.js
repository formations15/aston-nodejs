const f1 = (name) => {
  console.log("hey " + name);
};

const f2 = function (name) {
  console.log("hey " + name);
};

f1("testf1");
f2("testf2");

class Vehicule {
  roule() {
    console.log("roule");
  }
}

class Voiture extends Vehicule {
  constructor(x, y) {
    super();
    this.x = x;
    this.y = y;
  }
}
let voiture = new Voiture(5, 4);
voiture.roule();
console.log(voiture.x);
