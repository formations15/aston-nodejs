function printString(string, callback) {
  setTimeout(
    function () {
      console.log(string);
      if (callback) callback();
    },

    Math.floor(Math.random() * 100) + 1
  );
}

function printAll() {
  printString("A", () => {
    printString("B", () => {
      printString("C");
    });
  });
}
printAll();
