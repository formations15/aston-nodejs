const test = require("./test");
function printString(string) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log(string);
      resolve();
    }, Math.floor(Math.random() * 100) + 1);
  });
}

function printAll() {
  printString("A")
    .then(() => {
      return printString("B");
    })
    .then(() => {
      return printString("C");
    });
}

async function printAllWithAwait() {
  await printString("A");
  await printString("B");
  await printString("C");
}

printAll();

printAllWithAwait();

const myFirstPromise = new Promise(function (resolve, reject) {
  const condition = true;
  if (condition) {
    setTimeout(function () {
      resolve("Promise is resolved!");
    }, 300);
  } else {
    reject("Promise is rejected!");
  }
});

myFirstPromise
  .then((successMsg) => {
    console.log(successMsg);
  })
  .catch((errorMsg) => {
    console.log(errorMsg);
  });
