const printString = async (string) => {
  setTimeout(() => {
    console.log(string);
  }, Math.floor(Math.random() * 100) + 1);
};

function printAll() {
  printString("A");
  // 2000;
  printString("B");
  // 1000;
  printString("C");
  // 3000;
}
printAll();
