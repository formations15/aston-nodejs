const fs = require("fs");

const filepath = "text.txt";

let x = 5;
let y = 10;

// Prints mul x,y
console.log("mul", x * y);

// Reads a file in a synchronous and blocking way
const data = fs.readFileSync(filepath, { encoding: "utf8" });

// Prints the content of file
console.log(data);

// Prints sum x,y
console.log("sum", x + y);
