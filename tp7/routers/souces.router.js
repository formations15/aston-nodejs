const express = require("express");
const sauceRouter = express.Router();
const SauceController = require("../controllers/sauce.controller");

const API_SAUCES_PARAM = `/:id`;
const API_SAUCES_QUERY = `/search`;

// Begin Router
sauceRouter
  .route("/")
  .get(SauceController.findAll)
  .post(SauceController.create);

sauceRouter.route(API_SAUCES_QUERY).get(SauceController.findByName);

sauceRouter
  .route(API_SAUCES_PARAM)
  .get(SauceController.findById)
  .put(SauceController.update)
  .delete(SauceController.delete);

// end Router
module.exports = sauceRouter;
