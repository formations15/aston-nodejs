const express = require("express");
const router = express.Router();
const SauceRouter = require("./souces.router");
const API_SAUCES = `/sauces`;

router.use(API_SAUCES, SauceRouter);

module.exports = router;
