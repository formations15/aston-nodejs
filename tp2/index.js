const express = require("express");
const app = express();
const mocks = require("./mocks/sauces.js");

app.get("/sauces", (req, res, next) => {
  res.status(200).json({ sauces: mocks.SAUCES });
});

app.listen(process.env.PORT || "3000", () => {
  console.log("Le serveur est à l’écoute sur le port 3000");
});
