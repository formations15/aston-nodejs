const db = require("../models/index");
const User = db.users;

const UserRepository = {
  findAll: function () {
    return User.findAll();
  },
  createUser: function (user) {
    return User.create(user);
  },
  findByUsername: function (username) {
    const user = User.findOne({ where: { username: username } });
    return user;
  },
};

module.exports = UserRepository;
