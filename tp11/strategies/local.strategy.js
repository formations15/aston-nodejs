const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const UserService = require("../services/user.service");
const bcrypt = require("bcryptjs");

// Local Strategy
passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.use(
  new LocalStrategy(async (username, password, done) => {
    // verify user

    try {
      const user = await UserService.findByUsername(username);

      if (user) {
        // verify password
        const isMatch = await bcrypt.compare(password, user.password);
        if (isMatch) {
          return done(null, user);
        } else {
          return done(null, false, { message: "wrong password" });
        }
      } else {
        done(null, false, { message: "user not found" });
      }
    } catch (error) {
      done(error, false, { message: error });
    }
  })
);
