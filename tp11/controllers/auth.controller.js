const JwtHelper = require("../helpers/jwt.helpers");

const AuthController = {
  login: async (req, res, next) => {
    const loggedUser = req.user.dataValues;
    loggedUser.password = null;
    try {
      const access_token = await JwtHelper.getAcessToken(loggedUser);
      res.status(200).json({ access_token: access_token });
      return;
    } catch (error) {
      res.status(500).json({ meessage: "internal server error" });
      return;
    }
  },
};

module.exports = AuthController;
