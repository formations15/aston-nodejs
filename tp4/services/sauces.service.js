const SauceRepository = require("../repositories/sauces.repository");

const SaucesServices = {
  // return result with callback
  getSauces: (callback) => {
    return SauceRepository.getSauces((result) => {
      callback(result);
    });
  },
  // handle promise with .then .catch and return result with callback
  findById: (id, callback) => {
    let sauce;
    SauceRepository.findById(id)
      .then((result) => callback(result))
      .catch((err) => console.log(err)); // i/O event
    return sauce;
  },
  // handle promise with async await and return result
  findByName: async (name) => {
    try {
      const sauce = await SauceRepository.findByName(name); // I/O event
      return sauce;
    } catch (error) {
      console.log(error);
    }
  },
  createSauce: () => {},
  updateSauce: () => {},
  deleteSauce: () => {},
};

module.exports = SaucesServices;
