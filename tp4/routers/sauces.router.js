const express = require("express");
const SaucesController = require("../controllers/sauces.controller");
const sauceRouter = express.Router();
//POST /sauces
sauceRouter.route("/").get(SaucesController.getSauces);
//. post

//. put 
//. delete
sauceRouter.route("/search").get(SaucesController.findByName);

sauceRouter.route("/:id").get(SaucesController.findById);
// PUT/ sauces/:id
// DELETE /sauces/:id

module.exports = sauceRouter;
