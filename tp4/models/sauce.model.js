const Sauce = function (sauce) {
  this.name = sauce.name;
  this.note = sauce.note;
  this.description = sauce.description;
};

module.exports = Sauce;
