const mocks = require("../mocks/sauces");
const db = require("../models/db");

const TABLE_NAME = "sauces";

const SaucesRepository = {
  // with callback
  getSauces: (callback) => {
    let query = `SELECT * from ${TABLE_NAME}`;
    db.query(query, (err, result) => {
      if (err) {
        console.log(err);
        throw err;
      } else {
        callback(result);
      }
    });
  },
  // With promise
  findById: (id) => {
    let promise = new Promise((resolve, reject) => {
      let query = `SELECT * FROM ${TABLE_NAME} WHERE id = ${id}`;
      db.query(query, (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
          throw err;
        } else {
          if (result.length) {
            resolve(result);
          }
        }
      });
    });
    return promise;
  },
  // with promise
  findByName: (name) => {
    let promise = new Promise((resolve, reject) => {
      let query = `SELECT * FROM ${TABLE_NAME} WHERE name = '${name}'`;
      db.query(query, (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
          throw err;
        } else {
          if (result.length) {
            resolve(result);
          }
        }
      });
    });

    return promise;
  },
  createSauce: () => {},
  updateSauce: () => {},
  deleteSauce: () => {},
};

module.exports = SaucesRepository;
