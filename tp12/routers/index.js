const express = require("express");
const router = express.Router();
const SauceRouter = require("./souces.router");
const UsersRouter = require("./users.router");
const AuthRouter = require("../routers/auth.router");

const API_SAUCES = `/sauces`;
const API_USERS = `/users`;
const API_AUTH = `/auth`;

router.use(API_SAUCES, SauceRouter);
router.use(API_USERS, UsersRouter);
router.use(API_AUTH, AuthRouter);

module.exports = router;
