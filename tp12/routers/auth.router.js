const express = require("express");
const AuthRouter = express.Router();
const passport = require("passport");
const AuthController = require("../controllers/auth.controller");

AuthRouter.route("/login").post(
  passport.authenticate("local"), // req => req.user
  AuthController.login
);

module.exports = AuthRouter;
