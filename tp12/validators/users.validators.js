const { Joi } = require("express-validation");

const UsersValidators = {
  validateCreation: {
    body: Joi.object({
      username: Joi.string().max(80).required(),
      password: Joi.string().max(80).required(),
      role: Joi.string().valid("USER", "ADMIN"),
    }),
  },
};

module.exports = UsersValidators;
