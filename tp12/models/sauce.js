module.exports = (sequelize, Sequelize) => {
  const Sauce = sequelize.define("sauce", {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING,
    },
    note: {
      type: Sequelize.INTEGER,
      validate: {
        min: 0,
        max: 10,
      },
    },
    description: {
      type: Sequelize.STRING,
    },
  });

  return Sauce;
};
