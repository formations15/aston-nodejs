const UserRepository = require("../repositories/user.repository");
const bcrypt = require("bcryptjs");
const DuplicatedUserError = require("../errors/duplicatedUserError");

const UserService = {
  findAll: async function () {
    const users = await UserRepository.findAll();
    dto = [];
    if (users) {
      users.filter((user) => {
        dto.push({
          id: user.id,
          username: user.username,
          role: user.role,
        });
      });
    }
    return dto;
  },
  createUser: async function (user) {
    // verify if the username is already used
    const exist = await this.exists(user.username);
    if (exist) {
      throw new DuplicatedUserError();
    } else {
      cryptedPassword = await bcrypt.hash(user.password, 10);
      user.password = cryptedPassword;
      return await UserRepository.createUser(user);
    }
  },
  exists: async function (username) {
    const user = await UserRepository.findByUsername(username);
    return user != null;
  },
  findByUsername: async function (username) {
    const user = await UserRepository.findByUsername(username);
    return user;
  },
  findById: async function (id) {
    const user = await UserRepository.findById(id);
    return user;
  },
};

module.exports = UserService;
