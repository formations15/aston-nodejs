const express = require("express");
const app = express();
const db = require("./models/db");
const router = require("./routers");

// create table
// query
// let query =
//   "CREATE TABLE sauces (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(30), note INT , description VARCHAR(255))";

// db.query(query, (err, result) => {
//   if (err) {
//     console.log(err);
//     throw err;
//   } else {
//     console.log("table created");
//   }
// });

app.use(express.json());
app.use("/api", router);

app.get("*", (req, res, next) => {
  res.status(404).json({ message: "api not found" });
  return;
});

app.listen(process.env.PORT || "3000", () => {
  if (process.env.PORT)
    console.log(`le serveur est à l'ecoute sur le port ${process.env.PORT}`);
  else console.log(`le serveur est à l'ecoute sur le port 3000`);
});
