const mocks = require("../mocks/sauces");
const db = require("../models/db");

const TABLE_NAME = "sauces";

const SaucesRepository = {
  // with callback
  getSauces: (callback) => {
    let query = `SELECT * from ${TABLE_NAME}`;
    db.query(query, (err, result) => {
      if (err) {
        console.log(err);
        throw err;
      } else {
        callback(result);
      }
    });
  },
  // With promise
  findById: (id) => {
    let promise = new Promise((resolve, reject) => {
      let query = `SELECT * FROM ${TABLE_NAME} WHERE id = ${id}`;
      db.query(query, (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
          throw err;
        } else {
          if (result.length) {
            resolve(result);
          } else {
            resolve(null);
          }
        }
      });
    });
    return promise;
  },
  // with promise
  findByName: (name) => {
    let promise = new Promise((resolve, reject) => {
      let query = `SELECT * FROM ${TABLE_NAME} WHERE name = '${name}'`;
      db.query(query, (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
          throw err;
        } else {
          if (result.length) {
            resolve(result);
          }
        }
      });
    });

    return promise;
  },
  createSauce: (sauce) => {
    let promise = new Promise((resolve, reject) => {
      let query = `INSERT INTO ${TABLE_NAME} SET ?`;
      db.query(query, sauce, (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
          return;
        }
        resolve({ id: result.insertId, ...sauce });
      });
    });

    return promise;
  },
  updateSauce: (id, sauce) => {
    let promise = new Promise((resolve, reject) => {
      let query = `UPDATE ${TABLE_NAME} SET name = ? , note = ?, description = ? WHERE id = ?`;
      db.query(
        query,
        [sauce.name, sauce.note, sauce.description, id],
        (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve({ id: id, ...sauce });
          }
        }
      );
    });
    return promise;
  },
  deleteSauce: (id) => {
    let promise = new Promise((resolve, reject) => {
      let query = `DELETE FROM ${TABLE_NAME} WHERE id = ${id}`;
      db.query(query, (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
          throw err;
        }

        console.log("sauce delected successfully");
        resolve("sauce delected successfully");
      });
    });
    return promise;
  },
};

module.exports = SaucesRepository;
