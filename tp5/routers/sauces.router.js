const express = require("express");
const SaucesController = require("../controllers/sauces.controller");
const sauceRouter = express.Router();
//POST /sauces
sauceRouter
  .route("/")
  .get(SaucesController.getSauces)
  .post(SaucesController.createSauce);
//. post

//. put
//. delete
sauceRouter.route("/search").get(SaucesController.findByName);

sauceRouter
  .route("/:id")
  .get(SaucesController.findById)
  .delete(SaucesController.deleteSauce)
  .put(SaucesController.updateSauce);

module.exports = sauceRouter;
